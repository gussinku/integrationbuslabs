Lab 1 – Create an integration exposing an HTTP endpoint and returns a JSON message containing employee information.

Create an application and a message flow
1.	Right click on the application development tab and choose New -> Application.
2.	Enter the name of the application INT0001_EmployeeAPI.
3.	The application should now appear on your application development tab.
4.	Right click on the application you just created and choose New -> Message Flow
5.	Enter the name of the message flow EmployeeLookup.
6.	Double click on the newly created message flow. You should see an empty canvas.
Add and HTTPInput node
1.	Browse the Palette under HTTP  and choose the HTTP Input node. Drag and drop this onto the canvas.
2.	Click once on the HTTP Input node and the properties window will appear under the canvas.
3.	Click the Description tab and enter a new node name: /api/employees
4.	Click on the Basic tab and enter a new Path suffix for URL: /api/employees
This is the URL suffix that you will use in order to access the http endpoint.
Add a Compute node for creating a response message
1.	Drag a and drop a Compute Node onto the canvas
2.	Double-click to open the code view. Replace the auto-generated code with the below:
 
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		CALL CopyMessageHeaders();
		SET OutputRoot.JSON.Data.person.employeeId = '1337';
		SET OutputRoot.JSON.Data.person.firstName = 'Johan';
		SET OutputRoot.JSON.Data.person.lastName = 'Lindahl';
		SET OutputRoot.JSON.Data.person.phoneNumber = '1234567';
		SET OutputRoot.JSON.Data.person.title = 'The Integrator';
		
		SET OutputLocalEnvironment = InputLocalEnvironment;
		
		RETURN TRUE;
	END;

	CREATE PROCEDURE CopyMessageHeaders() BEGIN
		DECLARE I INTEGER 1;
		DECLARE J INTEGER;
		SET J = CARDINALITY(InputRoot.*[]);
		WHILE I < J DO
			SET OutputRoot.*[I] = InputRoot.*[I];
			SET I = I + 1;
		END WHILE;
	END;
Save the document and go back to the canvas containing the message flow.


Add an HTTPReply Node so the response can be sent back to the initial requester
1.	Drag and drop a HTTPReply node onto the canvas
	
Connecting the terminals to finalize the message flow
1.	Click the Out terminal of the HTTP Input node, drag the arrow that appears to the In terminal of the Compute node and click once. You should now have a fixed arrow between the two nodes.
2.	Click Out terminal of the Compute node and drag the arrow to the In terminal of the HTTP Reply node and click once. You should now have a fixed arrow between the two nodes.
Deploy the application to your integration server
1.	Right-click your application in the application development view.
2.	Select Deploy
3.	A window should appear showing you your integration node and an integration server called default, select the integration server named default and click finish.
Test your new integration
1.	Download and install Postman (Postman is a plugin to chrome that is used for creating HTTP requests)
2.	Create a Collection named IntegrationBus
3.	Add a new GET request and enter the following URL http://localhost:<port>/api/employees
4.	To find out the port that you are using enter the following command in the Integration Bus Console : mqsireportproperties <integration_node_name> -e default -o HTTPConnector -r
5.	From the result find the Port property and copy that value to insert into the URL.
6.	Click Send and you should now get a json body containing the employee information you specified in the ESQL compute node.
