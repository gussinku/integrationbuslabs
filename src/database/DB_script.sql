CREATE TABLE employee_info (
    emp_id int NOT null primary key,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    phone VARCHAR(255),
    title VARCHAR(255),
    region VARCHAR(255),
    active varchar(255)

);

INSERT into employee_info (emp_id,first_name,last_name,phone,title)
 VALUES ('1345','johan','Lindal','123456789','The Integrator');

 INSERT into employee_info (emp_id,first_name,last_name,phone,title)
 VALUES ('6337','mark','jetson','2255555555','The System');

 INSERT into employee_info (emp_id,first_name,last_name,phone,title)
 VALUES ('4537','mons','jetson','66999696963558','The Logiv');



/*SELECT *FROM employee;

ALTER TABLE employee
ADD CONSTRAINT df_Region 
DEFAULT 'SWEDEN' FOR Region;
